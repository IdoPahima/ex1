#include "stack.h";

void initStack(stack* s) {
	if (s != nullptr) {
		s->stack = nullptr;
	}
}
void cleanStack(stack* s) {
	if (s->stack != nullptr) {
		cleanList(s->stack);
	}
	delete(s);
}
int pop(stack* s) {
	if (!s) {
		return -1;
	}
	else if (!s->stack) {
		return -1;
	}
	else {
		int ret = s->stack->value;
		s->stack = removeHead(s->stack);
		return ret;
	}
}
void push(stack* s, unsigned int element) {
	if (s) {
		s->stack = add(s->stack, element);
	}
	else {
		std::cout << "the stack is Undefined!!" << std::endl;
	}
}