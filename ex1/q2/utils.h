#ifndef UTILS_H
#define UTILS_H
#include "stack.h";
/*
this func reverse an arr.
input: nums, size.
output: none.
*/
void reverse(int* nums, unsigned int size);
/*
this func reverse an arr.
input: none.
output: arr.
*/
int* reverse10();

#endif // UTILS_H