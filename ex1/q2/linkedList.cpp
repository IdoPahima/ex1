#include "linkedList.h"
#include <iostream>;

linkedList* add(linkedList* head, unsigned int value) {
	linkedList* ret = new linkedList;
	ret->value = value;
	ret->next = head;
	return ret;
}
linkedList* removeHead(linkedList* head) {
	linkedList* saveNext = head->next;
	delete(head);
	return saveNext;
}
void cleanList(linkedList* head) {
	linkedList* temp = NULL;
	while (head != nullptr) {
		temp = head;
		head = head->next;
		delete(temp);
	}
}