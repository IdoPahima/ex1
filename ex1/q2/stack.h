#ifndef STACK_H
#define STACK_H

#include "linkedList.h";
#include <iostream>;
/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	linkedList* stack;
} stack;
/*
this func add an element to the stack
input: stack* s, unsigned int element.
output: none.
*/
void push(stack* s, unsigned int element);
/*
this func return the first value of the first element and remove it.
input: stack* s.
output: ret.
*/
int pop(stack* s); // Return -1 if stack is empty
/*
this func initialize the stack.
input: stack* s.
output: none.
*/
void initStack(stack* s);
/*
this func clean the memory of the stack.
input: stack* s.
output: none.
*/
void cleanStack(stack* s);

#endif // STACK_H