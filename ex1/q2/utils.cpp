#include "utils.h";

void reverse(int* nums, unsigned int size) {
	stack* s = new(stack);
	initStack(s);
	int i = 0;
	for (i = 0; i < size; i++) {
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++) {
		nums[i] = pop(s);
	}
	cleanStack(s);
}

int* reverse10() {
	int* arr = new int[10];
	int i = 0;
	std::cout << "Enter nums:\n" << std::endl;
	for (i = 0; i < 10; i++) {
		std::cin >> arr[i];
	}
	reverse(arr, 10);
	return arr;
}