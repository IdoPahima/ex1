#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct linkedList
{
	unsigned int value;
	linkedList* next;
} linkedList;

linkedList* add(linkedList* head, unsigned int value);
linkedList* removeHead(linkedList* head);
void cleanList(linkedList* head);
#endif