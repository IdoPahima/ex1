#include "queue.h";

#include <iostream>;

void initQueue(queue* q, unsigned int size) {
	if (q != nullptr) {
		q->arr = new unsigned int[size];
		q->maxSize = size;
		q->lastIndex = 0;
	}
}
void cleanQueue(queue* q) {
	if (q != nullptr) {
		int i = 0;
		delete[] q->arr;
		delete(q);
	}
	else {
		std::cout << "The queue is Undefined!!\n" << std::endl;
	}
}

void enqueue(queue* q, unsigned int newValue) {
	if (q != nullptr) {
		if (q->lastIndex >= q->maxSize) {
			std::cout << "The queue is full!! can't add more numbers!!\n" << std::endl;
		}
		else {
			q->arr[q->lastIndex] = newValue;
			q->lastIndex++;
		}
	}
	else {
		std::cout << "The queue is Undefined!! cant insert new values\n" << std::endl;
	}
}
int dequeue(queue* q) {
	int ret = 0;
	if (q != nullptr) {
		if (q->lastIndex == 0) {
			std::cout << "The queue is empty!! 0 returnd\n" << std::endl;
			return 0;
		}
		else {
			unsigned int retVal = q->arr[0];
			int i = 0;
			ret = q->arr[0];
			for (i = 1; i < q->lastIndex; i++) {
				q->arr[i - 1] = q->arr[i];
			}
			q->lastIndex--;
		}

	}
	else {
		std::cout << "The queue is Undefined! 0 returnd\n" << std::endl;
		return 0;
	}
	return ret;
}