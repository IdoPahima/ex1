#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	unsigned int* arr;
	int maxSize;
	int lastIndex;
} queue;
/*
this func initilaize the queue
input: queue* q, unsigned int size.
output: none.
*/
void initQueue(queue* q, unsigned int size);
/*
this func clean the memmory of the queue.
input: queue* q.
output: none.
*/
void cleanQueue(queue* q);
/*
this func insert numbers to the queue.
input: queue* q, unsigned int newValue.
output: none.
*/
void enqueue(queue* q, unsigned int newValue);
/*
this func returns the first value in the queue.
input: queue* q.
output: none.
*/
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif /* QUEUE_H */